<div align="center">
<p>
    <a href="https://benchling.com/organizations/acubesat/">Benchling 🎐🧬</a> &bull;
    <a href="https://gitlab.com/acubesat/documentation/cdr-public/-/blob/master/DDJF/DDJF_PL.pdf?expanded=true&viewer=rich">DDJF_PL 📚🧪</a> &bull;
    <a href="https://spacedot.gr/">SpaceDot 🌌🪐</a> &bull;
    <a href="https://acubesat.spacedot.gr/">AcubeSAT 🛰️🌎</a>
</p>
</div>

## Description

A repository to host code and data regarding molecular biology-related analysis with a focus on yeast strains, including growth analysis and bioinformatic pipelines. Here you will currently find experiment design files, code to analyze the growth of yeast strains, mathematical modeling examples, as well as various figures and `.drawio` files regarding yeast biology-centered parts of the AcubeSAT mission.

