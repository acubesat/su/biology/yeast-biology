# Load libraries
library(dplyr)
library(tidyverse)
library(ggplot2)
library(ggthemr)
library(viridis)
library(wesanderson)

# Set paths
data_path <-
data_filename <- "20220607_IK_Data_annotated"

# Set wd
setwd(data_path)

# Load data
data <- read.csv(file.path(data_path, file.path(data_filename, "csv", fsep = ".")), header = TRUE, stringsAsFactors = FALSE)

# Remove NAs
data <- na.omit(data) 

# Plotting
ggthemr('fresh')

# Choose color palettes
gp1 <- wes_palettes$Darjeeling1
gp2 <- wes_palettes$Darjeeling2

# Select data for plotting
data <- filter(data, Type == "Spores" & Trehalose == "Yes")

ggplot(data, aes(x = as.factor(Condition), y = as.numeric(Colonies), fill = Condition)) +
    geom_bar(stat="identity", position = position_dodge()) +
    facet_grid(rows = vars(State), cols = vars(Medium)) +
    labs(x = "Treatment", y = "Colony count") +
    theme(
        strip.text = element_text(face = "bold", color = viridis(1, begin = 0.5, end = 0.9, option = "D")[1]),
        legend.position = "bottom",
        legend.text = element_text(size = 16),  
        legend.title = element_text(size = 16),
        axis.text.x = element_text(hjust = 0.5, size = 16),
        axis.text.y = element_text(size = 14),
        axis.title.x = element_text(size = 20),
        axis.title.y = element_text(size = 20),
        panel.border = element_rect(color = "black", fill=NA, size=1),
        strip.text.x = element_text(face = "bold", size = 16, margin = margin(2, 2, 2, 2), hjust = 0.5, color = "azure4"),
        strip.text.y = element_text(face = "bold", size = 16, margin = margin(2, 2, 2, 2), hjust = 0.5, angle = -90, color = "azure4"),
        strip.background = element_rect(fill = '#FFFFFF',)
    ) +
    scale_fill_manual(values = wes_palette(n = 3, name = "Darjeeling1"))

# ggsave("20220607_Trehalose_Y.pdf", width = 12, height = 8)
