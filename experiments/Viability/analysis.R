# Load libraries
library(dplyr)
library(tidyverse)
library(ggplot2)
library(wesanderson)
library(ggthemr)
library(viridis)

# Set paths
data_path <-
data_filename <- "Results"

# Set wd
setwd(data_path)

# Load data
data <- read.csv(file.path(data_path, file.path(data_filename, "csv", fsep = ".")), header = TRUE, stringsAsFactors = FALSE)

# Calculate statistics
data_summary <- function(data, varname, groupnames){
  require(plyr)
  summary_func <- function(x, col){
    c(mean = mean(x[[col]], na.rm = TRUE),
      sd = sd(x[[col]], na.rm = TRUE))
  }
  data_sum<-ddply(data, groupnames, .fun = summary_func,
                  varname)
  data_sum <- rename(data_sum, c("mean" = varname))
  return(data_sum)
}

data_stats <- data_summary(data, varname = c("Colonies"),
                                          groupnames = c("Time", "Dilution", "State", "Date"))

# Renaming
data_stats$Dilution[data_stats$Dilution == "1:10^3"] <- "1:10\u00B3"

# Plotting

data_plot <- subset(data_stats, Dilution %in% "1:10\u00B3")

# Choose color palettes
gp1 <- wes_palettes$Darjeeling1
gp2 <- wes_palettes$Darjeeling2
ggthemr('fresh')

ggplot(data_plot, aes(x = as.factor(Time), y = as.numeric(Colonies))) +
    geom_bar(stat = "summary",
            fun.y = "mean") +
    geom_errorbar(aes(ymin = Colonies - sd, ymax = Colonies + sd), width=.2,
                 position = position_dodge(.9), color = "black") +
    # facet_grid(cols = vars(Dilution)) +
    labs(x = "Time (months)", y = "Colony count") +
    theme(
        strip.text = element_text(face = "bold", color = viridis(1, begin = 0.5, end = 0.9, option = "D")[1]),
        legend.position = "bottom",
        legend.text = element_text(size = 16),  
        legend.title = element_text(size = 16),
        axis.text.x = element_text(angle = 60, hjust = 1, size = 16),
        axis.text.y = element_text(size = 16),
        axis.title.x = element_text(size = 20),
        axis.title.y = element_text(size = 20),
        panel.border = element_rect(color = "black", fill=NA, size=1),
        strip.text.x = element_text(face = "bold", size = 14, margin = margin(2, 2, 2, 2), hjust = 0.5, color = "azure4"),
        strip.text.y = element_text(face = "bold", size = 14, margin = margin(2, 2, 2, 2), hjust = 0.5, angle = -90, color = "azure4"),
        strip.background = element_rect(fill = '#FFFFFF',)
    ) +
    scale_fill_viridis(discrete = TRUE) +
    scale_color_viridis(discrete = TRUE)

# ggsave("Viability_E3.pdf", width = 14, height = 8)
