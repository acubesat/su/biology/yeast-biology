# Load libraries
library(dplyr)
library(tidyverse)
library(ggplot2)
library(wesanderson)
library(ggthemr)
library(viridis)

# Set paths
data_path <- 
data_filename <- 
design_filename <- 

# Set timepoints
timepoints <- 36

# Set wd
setwd(data_path)

# Data preprocessing
source("data_preprocessing.R")
data_annotated <- data_preprocessing(data_path, data_filename, design_filename, timepoints)

# Select data for downstream analysis-plotting
data_analysis_no_spo <- filter(data_annotated, 
(Medium %in% c("SD-HIS") & Trehalose %in% c("No", "Sporulation Medium")))
data_analysis_growth <- filter(data_annotated, 
(Medium %in% c("SD-HIS") & Trehalose %in% c("Growth Medium")))

# Create df with OD of the blank media
data_blank_no <- filter(data_analysis_no_spo,
(Strain %in% c("Blank") & Replicate %in% c("1")))
data_blank_growth <- filter(data_analysis_growth,
(Strain %in% c("Blank") & Replicate %in% c("1")))

# Subtract OD of blank media from samples (to correct for medium absorbance)
data_analysis_no_spo %>% group_by(Well, Replicate, Strain, Trehalose, Condition, Medium) %>%
summarize(Time = Time, OD_corrected = 
as.numeric(OD) - as.numeric(data_blank_no$OD)) -> data_analysis_no_spo_corrected

data_analysis_growth %>% group_by(Well, Replicate, Strain, Trehalose, Condition, Medium) %>% 
summarize(Time = Time, 
OD_corrected = as.numeric(OD) - as.numeric(data_blank_growth$OD)) -> data_analysis_growth_corrected

data_analysis_corrected <- rbind(data_analysis_no_spo_corrected, data_analysis_growth_corrected)

# Calculate statistics
data_summary <- function(data, varname, groupnames){
  require(plyr)
  summary_func <- function(x, col){
    c(mean = mean(x[[col]], na.rm = TRUE),
      sd = sd(x[[col]], na.rm = TRUE))
  }
  data_sum<-ddply(data, groupnames, .fun = summary_func,
                  varname)
  data_sum <- rename(data_sum, c("mean" = varname))
  return(data_sum)
}

data_stats <- data_summary(data_analysis_corrected, varname = c("OD_corrected"),
                                          groupnames = c("Time", "Strain", "Trehalose", "Condition"))

# Factorize
data_stats$Trehalose <- factor(data_stats$Trehalose, levels = c("No", "Sporulation Medium", "Growth Medium"))
data_stats$Condition <- factor(data_stats$Condition, levels = c("RT", "Cold Shock", "Heat Shock"))

# Plotting

data_plot <- filter(data_stats, 
(Strain %in% c("Y7039 x YHL033C")))

# Choose color palettes
gp1 <- wes_palettes$Darjeeling1
gp2 <- wes_palettes$Darjeeling2
ggthemr('fresh')

# Set specific colors
condition_colors <- c("RT" = gp1[3], "Cold Shock" = gp1[2], "Heat Shock" = gp1[1])

ggplot(data_plot, aes(x = as.numeric(Time), y = as.numeric(OD_corrected), group = Condition)) +
    geom_ribbon(aes(ymin = OD_corrected - sd, ymax = OD_corrected + sd, fill = Condition), alpha = 0.2) +
    geom_point(aes(color = Condition)) +
    facet_grid(cols = vars(Trehalose)) +
    labs(x = "Time (hours)", y = expression(OD[600])) +
    theme(
        strip.text = element_text(face = "bold", color = viridis(1, begin = 0.5, end = 0.9, option = "D")[1]),
        legend.position = "bottom",
        legend.text = element_text(size = 16),  
        legend.title = element_text(size = 16),
        axis.text.x = element_text(angle = 60, hjust = 1, size = 16),
        axis.text.y = element_text(size = 16),
        axis.title.x = element_text(size = 20),
        axis.title.y = element_text(size = 20),
        panel.border = element_rect(color = "black", fill=NA, size=1),
        strip.text.x = element_text(face = "bold", size = 16, margin = margin(2, 2, 2, 2), hjust = 0.5, color = "azure4"),
        strip.text.y = element_text(face = "bold", size = 16, margin = margin(2, 2, 2, 2), hjust = 0.5, angle = -90, color = "azure4"),
        strip.background = element_rect(fill = '#FFFFFF',)
    ) +
    scale_color_manual(values = condition_colors) +
    scale_fill_manual(values = condition_colors)

# ggsave("20220524_DN_IK_SD_HIS.pdf", width = 14, height = 8)