# Load libraries
library(ggplot2)
library(ggcyto)
library(tidyverse)
library("data.table")
library(dplyr)
library(stringr)
library(RColorBrewer)
library(ggthemr)
library(viridis)

# Kickstart plot viewer
plot(1,2)

# Set path
data_path <- 
setwd(data_path)

# Read csv
statistics_df <- read.csv(file.path(data_path, "Statistics.csv"), sep=';', header=FALSE, stringsAsFactors=FALSE)

# Clean up the data-frame
statistics_df <- statistics_df[-1,]
statistics_df <- statistics_df[,-c(2,3)]

# Make first row the header
names(statistics_df) <- statistics_df[1,]
statistics_df <- statistics_df[-1,]

# More cleaning

colnames_to_remove <- c("Volume(μL):", "Abort(%):", "All Events Events/μL(V)", "P1 Events/μL(V)", "P2 Events/μL(V)", "P3 Events/μL(V)", "P4 Events/μL(V)", "P5 Events/μL(V)", "P6 Events/μL(V)")

statistics_df <- statistics_df[ , !(names(statistics_df) %in% colnames_to_remove)]
statistics_df <- statistics_df[,-16] # Remove All Events Events/μL(V) column

# Reshape data
statistics_long_df <- melt(statistics_df, id.vars="Tube Name:")

# Extract Population and Channel
statistics_long_df <- statistics_long_df %>%
  mutate(Population = case_when(
    grepl("All Events", variable) ~ "All",
    grepl("^P[1-6]", variable) ~ str_extract(variable, "^P[1-6]"),
    TRUE ~ NA_character_
  ),
  Channel = case_when(
    grepl("Mean", variable) ~ str_extract(variable, "Mean [A-Z]+-?[A-Z]*"),
    TRUE ~ NA_character_
  ))

# Extract % Total and % Parent
statistics_long_df <- statistics_long_df %>%
  mutate(`% Total` = ifelse(grepl("% Total", variable), "Yes", NA_character_),
         `% Parent` = ifelse(grepl("% Parent", variable), "Yes", NA_character_))

# Remove Population and Channel from % Total and % Parent if they are not NA
statistics_long_df <- statistics_long_df %>%
  mutate(Population = ifelse(!is.na(`% Total`) | !is.na(`% Parent`), str_extract(variable, "^P[1-6]|All"), Population))

# More cleaning

# 1. Filter out rows with 01-Well-E1 using the subset function
# statistics_long_df <- subset(statistics_long_df, `Tube Name:` != "01-Well-E1")

# 1. Rename columns
names(statistics_long_df)[names(statistics_long_df) == "variable"] <- "Variable"
names(statistics_long_df)[names(statistics_long_df) == "value"] <- "Value"
names(statistics_long_df)[names(statistics_long_df) == "% Total"] <- "Total_Percentage"
names(statistics_long_df)[names(statistics_long_df) == "% Parent"] <- "Parent_Percentage"

# 2. Remove word 'Mean' from Channel column
statistics_long_df$Channel <- gsub("Mean ", "", statistics_long_df$Channel)

# 3. Update the Population column
statistics_long_df$Population[statistics_long_df$Population == "P1"] <- "Live"
statistics_long_df$Population[statistics_long_df$Population == "P2"] <- "Singlets"
statistics_long_df$Population[statistics_long_df$Population == "P3"] <- "Vegetative"
statistics_long_df$Population[statistics_long_df$Population == "P4"] <- "Tetrads"
statistics_long_df$Population[statistics_long_df$Population == "P5"] <- "Singlets (stained)"
statistics_long_df$Population[statistics_long_df$Population == "P6"] <- "Tetrads (stained)"

# 4. Add Sample column based on `Tube Name:` column
statistics_long_df <- statistics_long_df %>%
  mutate(Sample = case_when(
    `Tube Name:` == "01-Well-B1" ~ "YCR012W x Y7039 spores",
    `Tube Name:` == "01-Well-B2" ~ "YKL152C x Y7039 spores",
    `Tube Name:` == "01-Well-B3" ~ "YCR012W",
    `Tube Name:` == "01-Well-B4" ~ "YKL152C",
    `Tube Name:` == "01-Well-C1" ~ "YCR012W x Y7039 spores",
    `Tube Name:` == "01-Well-C2" ~ "YKL152C x Y7039 spores",
    `Tube Name:` == "01-Well-C3" ~ "YCR012W",
    `Tube Name:` == "01-Well-C4" ~ "YKL152C",
    TRUE ~ NA_character_
  ))

# 5. Remove % symbol
statistics_long_df$Value<-gsub("%","",as.character(statistics_long_df$Value))

# 6. Define factor levels
statistics_long_df$Population <- factor(statistics_long_df$Population, levels = c("All", "Live", "Singlets", "Vegetative", "Tetrads", "Singlets (stained)", "Tetrads (stained)"))
statistics_long_df$Sample <- factor(statistics_long_df$Sample, levels = c("YKL152C","YCR012W", "YCR012W x Y7039 spores"))

# 7. Add a `Staining` column
statistics_long_df$Staining <- ifelse(statistics_long_df$`Tube Name:` %in% c("01-Well-B1", "01-Well-B2", "01-Well-B3", "01-Well-B4"), "PI staining",
                      ifelse(statistics_long_df$`Tube Name:` %in% c("01-Well-C1", "01-Well-C2", "01-Well-C3", "01-Well-C4"), "No staining", NA))


# Select data for plotting
statistics_plot_df <- subset(statistics_long_df, Total_Percentage == "Yes" & Sample != "YKL152C x Y7039 spores") # YKL152C x Y7039 spores had hyphae)

# statistics_plot_df <- subset(statistics_long_df, Channel == "FITC-A")

# Plotting
ggthemr('fresh')

# Population 

ggplot(data = statistics_plot_df, aes(x = Population, y = as.numeric(Value), fill = Sample)) +
  # geom_bar(aes(alpha=Sample),stat = "summary", fun.y = "mean",fill="palegreen3",color="black") +
  geom_bar(stat = "summary", fun.y = "mean", color="black") +
  # geom_point(aes(color=Replicate)) +
  facet_grid(cols = vars(Sample), rows = vars(Staining)) +
  labs(x = "Strain", y = "Population (%)") +
  theme(
        strip.text = element_text(face = "bold", color = viridis(1, begin = 0.5, end = 0.9, option = "D")[1]),
        legend.position = "bottom",
        legend.text = element_text(size = 16),  
        legend.title = element_text(size = 16),
        axis.text.x = element_text(angle = 60, hjust = 1, size = 16),
        axis.text.y = element_text(size = 16),
        axis.title.x = element_text(size = 20),
        axis.title.y = element_text(size = 20),
        panel.border = element_rect(color = "black", fill=NA, size=1),
        strip.text.x = element_text(face = "bold", size = 16, margin = margin(2, 2, 2, 2), hjust = 0.5, color = "azure4"),
        strip.text.y = element_text(face = "bold", size = 16, margin = margin(2, 2, 2, 2), hjust = 0.5, angle = -90, color = "azure4"),
        strip.background = element_rect(fill = '#FFFFFF',)
    ) +
    scale_fill_viridis(discrete = TRUE) +
    scale_color_viridis(discrete = TRUE)

# ggsave("AA_20230905_Population.pdf", width = 12, height = 10)


# Select data for plotting
statistics_plot_df <- subset(statistics_long_df, Channel != "NA" & Channel == "FITC-A" & Population != "Singlets (stained)" & Population != "Tetrads (stained)" & Sample != "YKL152C x Y7039 spores") # YKL152C x Y7039 spores had hyphae))

# mNeonGreen 

ggplot(data = statistics_plot_df, aes(x = Population, y = as.numeric(Value), fill = Sample)) +
  # geom_bar(aes(alpha=Sample),stat = "summary", fun.y = "mean",fill="palegreen3",color="black") +
  geom_bar(stat = "summary", fun.y = "mean", color="black") +
  # geom_point(aes(color=Replicate)) +
  facet_grid(cols = vars(Sample), rows = vars(Staining)) +
  labs(x = "Strain", y = "mNeonGreen (a.u.)") +
  theme(
        strip.text = element_text(face = "bold", color = viridis(1, begin = 0.5, end = 0.9, option = "D")[1]),
        legend.position = "bottom",
        legend.text = element_text(size = 16),  
        legend.title = element_text(size = 16),
        axis.text.x = element_text(angle = 60, hjust = 1, size = 16),
        axis.text.y = element_text(size = 16),
        axis.title.x = element_text(size = 20),
        axis.title.y = element_text(size = 20),
        panel.border = element_rect(color = "black", fill=NA, size=1),
        strip.text.x = element_text(face = "bold", size = 16, margin = margin(2, 2, 2, 2), hjust = 0.5, color = "azure4"),
        strip.text.y = element_text(face = "bold", size = 16, margin = margin(2, 2, 2, 2), hjust = 0.5, angle = -90, color = "azure4"),
        strip.background = element_rect(fill = '#FFFFFF',)
    ) +
    scale_fill_viridis(discrete = TRUE) +
    scale_color_viridis(discrete = TRUE)

# ggsave("AA_20230905_mNeonGreen.pdf", width = 12, height = 10)

# Select data for plotting
statistics_plot_df <- subset(statistics_long_df, Channel != "NA" & Channel == "ECD-A" & Population != "Singlets (stained)" & Population != "Tetrads (stained)" & Sample != "YKL152C x Y7039 spores") # YKL152C x Y7039 spores had hyphae)))

# Propidium iodide 

ggplot(data = statistics_plot_df, aes(x = Population, y = as.numeric(Value), fill = Sample)) +
  # geom_bar(aes(alpha=Sample),stat = "summary", fun.y = "mean",fill="palegreen3",color="black") +
  geom_bar(stat = "summary", fun.y = "mean", color="black") +
  # geom_point(aes(color=Replicate)) +
  facet_grid(cols = vars(Sample), rows = vars(Staining)) +
  labs(x = "Strain", y = "ECD (a.u.)") +
  theme(
        strip.text = element_text(face = "bold", color = viridis(1, begin = 0.5, end = 0.9, option = "D")[1]),
        legend.position = "bottom",
        legend.text = element_text(size = 16),  
        legend.title = element_text(size = 16),
        axis.text.x = element_text(angle = 60, hjust = 1, size = 16),
        axis.text.y = element_text(size = 16),
        axis.title.x = element_text(size = 20),
        axis.title.y = element_text(size = 20),
        panel.border = element_rect(color = "black", fill=NA, size=1),
        strip.text.x = element_text(face = "bold", size = 16, margin = margin(2, 2, 2, 2), hjust = 0.5, color = "azure4"),
        strip.text.y = element_text(face = "bold", size = 16, margin = margin(2, 2, 2, 2), hjust = 0.5, angle = -90, color = "azure4"),
        strip.background = element_rect(fill = '#FFFFFF',)
    ) +
    scale_fill_viridis(discrete = TRUE) +
    scale_color_viridis(discrete = TRUE)

# ggsave("AA_20230905_PI.pdf", width = 12, height = 10)

# view(test[sample(1:nrow(test), 15), ])

# clipr::write_clip(test[sample(1:nrow(test), 15), ])

# clipr::write_clip(test)